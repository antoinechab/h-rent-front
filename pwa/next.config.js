/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["hesias-rent.herokuapp.com","hesias-rent.herokuapp.com"],
  },
};

module.exports = nextConfig;
