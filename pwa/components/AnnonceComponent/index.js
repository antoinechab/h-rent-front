import DefaultPicture from "../../assets/accueilbis.jpg";
import styles from "./AnnonceComponent.module.css";
import Image from "next/image";
import Link from "next/link";

export default function AnnonceComponent({ annonce }) {
  return (
    <div className={styles.wrapper}>
      <Link href={`/annonces/${annonce.id}`}>
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-md-4">
              <Image
                src={DefaultPicture}
                alt="..."
              />
            </div>
            <div className="col-md-8">
              <div className="card-body">
                <h3 className="card-title">{annonce.title}</h3>
                <p className="card-text" style={{ height: "70px", overflow: 'hidden' }}>
                  {annonce.description}
                </p>
                <p className="card-text">
                  Capacité: {annonce.capacity}
                </p>
                <p className="card-text">
                  <small className="text-muted">
                    {annonce.address.city}, {annonce.address.zipCode}
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
