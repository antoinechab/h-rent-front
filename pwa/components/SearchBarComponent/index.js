import Image from "next/image";
import { useRouter } from "next/router";
import arrowright from "../../assets/arrow-right.svg";
import { useEffect, useState } from "react";

export default function SearchBarComponent() {
  const [title, setTitle] = useState("");
  const [city, setCity] = useState("");
  const [distance, setDistance] = useState(5);
  const [capacity, setCapacity] = useState();
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const router = useRouter();

  useEffect(() => {
    setTitle(router.query.title);
    setCity(router.query.city);
    setDistance(router.query.distance);
    setCapacity(router.query.capacity);
    setStartDate(router.query.startDate);
    setEndDate(router.query.endDate);
  }, [router]);

  return (
    <>
      <form className="mb-5 mt-5 row justify-content-center" action="/annonces">
        <input
          className="col-2 form-control"
          type="text"
          placeholder="Titre"
          id="title"
          name="title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          className="col-1 form-control"
          type="text"
          placeholder="Ville"
          id="city"
          name="city"
          value={city}
          onChange={(e) => setCity(e.target.value)}
        />
        <select
          className="form-select"
          id="distance"
          name="distance"
          value={distance}
          onChange={(e) => setDistance(e.target.value)}
        >
          <option value="5">5 Km</option>
          <option value="10">10 Km</option>
          <option value="50">50 Km</option>
          <option value="100">100 Km</option>
        </select>
        <input
          type="number"
          name="capacity"
          className="col-1 form-control"
          placeholder="Capacité"
          id="capacity"
          min="0"
          value={capacity}
          onChange={(e) => setCapacity(e.target.value)}
        />
        <input
          className="col-1 form-control"
          type="date"
          id="startDate"
          name="startDate"
          value={startDate}
          onChange={(e) => setStartDate(e.target.value)}
        />
        <Image src={arrowright} alt="" />
        <input
          className="col-1 form-control"
          type="date"
          id="endDate"
          name="endDate"
          value={endDate}
          onChange={(e) => setEndDate(e.target.value)}
        />
        <button className="btn btn-primary" type="submit">
          Rechercher
        </button>
      </form>
    </>
  );
}
