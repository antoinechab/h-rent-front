import { useState, useEffect } from "react";
import styles from "./SlotModal.module.css";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";

const SlotModal = ({
  show,
  onClose,
  onCreate,
  annonceId,
  accessToken,
  slots,
}) => {
  const [isBrowser, setIsBrowser] = useState(false);

  const [price, setPrice] = useState(0);

  const [disabledDates, setDisabledDates] = useState([]);

  const [selectionRange, setSelectionRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: "selection",
  });

  const handleSelectDate = (ranges) => {
    selectionRange.startDate = ranges.selection.startDate;
    selectionRange.endDate = ranges.selection.endDate;
  };

  useEffect(() => {
    const dd = [];
    for (let slot of slots) {
      dd.push(new Date(slot.startDate));
    }
    setDisabledDates(dd);
    setIsBrowser(true);
  }, [slots]);

  const handleCloseClick = (event) => {
    event.preventDefault();
    onClose();
  };

  const handleCreateSlot = async (event) => {
    event.preventDefault();

    const selectedSlots = [];

    let currentDate = new Date(
      new Date(selectionRange.startDate).getFullYear(),
      new Date(selectionRange.startDate).getMonth(),
      new Date(selectionRange.startDate).getDate()
    );

    while (currentDate <= new Date(selectionRange.endDate)) {
      selectedSlots.push(currentDate);

      currentDate = new Date(
        new Date(currentDate).getFullYear(),
        new Date(currentDate).getMonth(),
        new Date(currentDate).getDate() + 1 // Will increase month if over range
      );
    }

    const annonce = `/annonces/${annonceId}`;

    for (let date of selectedSlots) {
      let start = new Date(date.setHours(12, 0, 0));
      let end = new Date(date.setHours(23, 59, 59));

      let response = await fetch("https://hesias-rent.herokuapp.com/slots", {
        method: "POST",
        body: JSON.stringify({
          annonce,
          startDate: start,
          endDate: start,
          price: Number(price),
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      });

      const slotData = await response.json();
    }

    onCreate();
  };

  const modalContent = show ? (
    <div className={styles.overlay}>
      <div className={styles.modal}>
        <div className={styles.header}>
          <h5>Ajout d&apos;un créneau</h5>
          <a href="#" onClick={handleCloseClick}>
            x
          </a>
        </div>
        <form onSubmit={handleCreateSlot} method="post" className="mt-1 ml-3">
          <div className="mt-2">
            <label>Période</label>
            <DateRange
              ranges={[selectionRange]}
              onChange={handleSelectDate}
              minDate={new Date()}
              disabledDates={disabledDates}
            />
          </div>
          <div className="mt-2">
            <label className="form-label">Prix</label>
            <input
              className="col-5 form-control"
              type="number"
              id="price"
              min="0"
              required
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
          <div className="mt-5">
            <button className="mr-5 btn-secondary" onClick={handleCloseClick}>
              Annuler
            </button>
            <button className="btn btn btn-primary" type="submit">
              Valider
            </button>
          </div>
        </form>
      </div>
    </div>
  ) : null;

  if (isBrowser) {
    return modalContent;
  } else {
    return null;
  }
};

export default SlotModal;
