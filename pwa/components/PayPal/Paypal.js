import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";

export default function Paypal({
  booking,
  price,
  onPaymentValidate,
  accessToken,
}) {
  async function onSuccess(payment, statut) {
    console.log("Payment success:", payment, statut);
    const res = await fetch("https://hesias-rent.herokuapp.com/payements", {
      method: "POST",
      body: JSON.stringify({
        paypalResponse: payment,
        statut: 1,
        booking: `/bookings/${booking.id}`,
      }),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    });
    console.log(res);
    if (res.ok) {
      console.log("ok");
      onPaymentValidate();
    }
  }

  return (
    <PayPalScriptProvider
      options={{
        "client-id":
          "Abrhkm-CFn2lM8wnDYnkYYL8uslJdkq3bFAGLqQLSgb-EgXAlSC0aAtpcYCIr9kttYocTKTL22Rn8CAb",
        currency: "EUR",
      }}
    >
      <PayPalButtons
        createOrder={(data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: price,
                },
              },
            ],
          });
        }}
        onApprove={(data, actions) => {
          return actions.order.capture().then((details) => {
            onSuccess(details, details.status);
          });
        }}
      />
    </PayPalScriptProvider>
  );
}
