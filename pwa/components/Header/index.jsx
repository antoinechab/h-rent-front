import Link from "next/link";
import { signIn, signOut, useSession } from "next-auth/react"



export default function Header() {
    const { data, status } = useSession();

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link href="/">
                <div >
                    <a className="navbar-brand" style={{ cursor: "pointer" }}>H-Rent</a>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>
            </Link>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link href="/">
                            <a className="nav-link">Accueil</a>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link href="/annonces?page=1">
                            <a className="nav-link">Liste des annonces</a>
                        </Link>
                    </li>
                    {status == "authenticated" &&
                        <li className="nav-item">
                            <Link href="/creation_annonce">
                                <a className="nav-link">Ajouter une annonce</a>
                            </Link>
                        </li>
                    }
                </ul>
                {status == "authenticated" ?
                    <ul className="form-inline navbar-nav">
                        <li className="nav-item">
                            <Link href="/profile/informations">
                                <a className="nav-link">Compte</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <a onClick={() => signOut({ callbackUrl: '/' })} className="nav-link" style={{ cursor: 'pointer' }}>Deconnexion</a>
                        </li>
                    </ul>
                    :
                    <ul className="form-inline navbar-nav">
                        <li className="nav-item">
                            <a onClick={() => signIn("credentials", { callbackUrl: '/' })} className="nav-link" style={{ cursor: 'pointer' }}>Se connecter</a>
                        </li>
                        <li className="nav-item">
                            <Link href="/register">
                                <a className="nav-link">Créer un compte</a>
                            </Link>
                        </li>

                    </ul>
                }
            </div>
        </nav >
    );
}
