import styles from './Filter.module.css'
import { useState } from 'react';


export default function Filter() {
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(0);
  const [nbPeople, setNbPeople] = useState(0);
  const [startDate, setStartDate] = useState(0);
  const [endDate, setEndDate] = useState(0);

  const handleSubmit = (event) => {
    if (minPrice > maxPrice) {
      alert("Le prix minimum doit être plus élevé que le prix minimum");
      event.preventDefault();
      return;
    }
    if (startDate > endDate) {
      alert("Erreur dans la saisie des dates")
      event.preventDefault();
      return;
    }
    console.log(`
      minPrice: ${minPrice}
      maxPrice: ${maxPrice}
      nbPeople: ${nbPeople}
      startDate: ${startDate}
      endDate: ${endDate}
      `);


    event.preventDefault();
  }

  return (
    <div className={styles.container}>
      <h3>
        Selection des filtres
      </h3>

      <form onSubmit={handleSubmit} className={styles.form}>

        <label className={styles.label}>
          Prix minimum
          <input
            name="minPrice"
            type="number"
            value={minPrice}
            onChange={e => setMinPrice(e.target.value)}
          />
        </label>

        <label className={styles.label}>
          Prix maximum
          <input
            name="maxPrice"
            type="number"
            value={maxPrice}
            onChange={e => setMaxPrice(e.target.value)} />
        </label>

        <label className={styles.label}>
          Nombre de personnes
          <input
            name="nbPeople"
            type="number"
            value={nbPeople}
            onChange={e => setNbPeople(e.target.value)}
          />
        </label>

        <label className={styles.label}>
          Date de début
          <input
            name="startDate"
            type="date"
            value={startDate}
            onChange={e => setStartDate(e.target.value)}
          />
        </label>

        <label className={styles.label}>
          Date de fin
          <input
            name="endDate"
            type="date"
            value={endDate}
            onChange={e => setEndDate(e.target.value)}
          />
        </label>


        <button className={styles.submit}>Submit</button>

      </form>
    </div>
  )
}
