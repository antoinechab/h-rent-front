import { useState, useEffect } from "react";
import styles from "./BookingModal.module.css";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";

const BookingModal = ({
  show,
  onClose,
  annonce,
  myId,
  accessToken,
  slots,
  onValidate,
}) => {
  const [isBrowser, setIsBrowser] = useState(false);
  const [disabledDates, setDisabledDates] = useState([]);
  const [price, setPrice] = useState(0);
  const [selectionRange, setSelectionRange] = useState({
    startDate: new Date(),
    endDate: new Date(),
    key: "selection",
  });
  const [slotUsed, setSlotUsed] = useState([]);

  const handleSelectDate = (ranges) => {
    selectionRange.startDate = ranges.selection.startDate;
    selectionRange.endDate = ranges.selection.endDate;
    calculatePrice();
  };

  const calculatePrice = async () => {
    var total = 0;

    let bookingDays = [];

    let currentDate = new Date(
      new Date(selectionRange.startDate).getFullYear(),
      new Date(selectionRange.startDate).getMonth(),
      new Date(selectionRange.startDate).getDate()
    );

    while (currentDate <= new Date(selectionRange.endDate)) {
      bookingDays.push(currentDate);

      currentDate = new Date(
        new Date(currentDate).getFullYear(),
        new Date(currentDate).getMonth(),
        new Date(currentDate).getDate() + 1
      );
    }

    const slotss = [];

    for (let slot of slots) {
      let start = new Date(new Date(slot.startDate).setHours(0, 0, 0));
      let end = new Date(new Date(slot.startDate).setHours(23, 59, 59));
      for (let date of bookingDays) {
        let testedDay = new Date(date);

        if (start <= testedDay && end >= testedDay) {
          total = total + slot.price;
          slotss.push(slot);
        }
      }
    }

    setPrice(total);
    setSlotUsed(slotss);
  };

  useEffect(() => {
    const actualDate = new Date();

    const sixMonth = new Date(
      actualDate.getFullYear(),
      actualDate.getMonth() + 6,
      actualDate.getDate()
    );

    const sixMonthInterval = [];

    while (actualDate <= new Date(sixMonth)) {
      sixMonthInterval.push(actualDate);

      actualDate = new Date(
        new Date(actualDate).getFullYear(),
        new Date(actualDate).getMonth(),
        new Date(actualDate).getDate() + 1
      );
    }
    sixMonthInterval.map((date, index) => {
      let before = date;
      let after = date;
      for (let slot of slots) {
        let testedDate = new Date(slot.startDate);

        if (
          new Date(testedDate.setHours(12, 0, 0)) >
            new Date(before.setHours(0, 0, 0)) &&
          new Date(testedDate.setHours(12, 0, 0)) <
            new Date(after.setHours(23, 59, 59))
        ) {
          sixMonthInterval[index] = null;
          return;
        }
      }
    });

    for (let booking of annonce.bookings) {
      let currentDate = new Date(
        new Date(booking.startDate).getFullYear(),
        new Date(booking.startDate).getMonth(),
        new Date(booking.startDate).getDate()
      );

      while (currentDate <= new Date(booking.endDate)) {
        sixMonthInterval.push(currentDate);

        currentDate = new Date(
          new Date(currentDate).getFullYear(),
          new Date(currentDate).getMonth(),
          new Date(currentDate).getDate() + 1 // Will increase month if over range
        );
      }
    }
    setDisabledDates(sixMonthInterval);
    setIsBrowser(true);
  }, [slots]);

  const handleCloseClick = (event) => {
    event.preventDefault();
    onClose();
  };

  const handleCreateBooking = async (event) => {
    event.preventDefault();

    let startAt = new Date(selectionRange.startDate.setHours(0, 0, 0));
    let createdAt = new Date();

    let slots = [];
    for (let slot of slotUsed) {
      slots.push(`/slots/${slot.id}`);
    }

    console.log(slots);

    let tenant = `/users/${myId}`;
    let nbPeople = annonce.capacity;

    console.log(
      JSON.stringify({
        createdAt,
        startAt,
        nbPeople,
        status: -1,
        slots,
        tenant,
        annonce: `/annonces/${annonce.id}`,
      })
    );

    let response = await fetch("https://hesias-rent.herokuapp.com/bookings", {
      method: "POST",
      body: JSON.stringify({
        createdAt,
        startAt,
        nbPeople,
        statut: -1,
        slots,
        tenant,
        annonce: `/annonces/${annonce.id}`,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    });

    const bookingData = await response.json();
    console.log(bookingData);

    onValidate();
  };

  const modalContent = show ? (
    <div className={styles.overlay}>
      <div className={styles.modal}>
        <div className={styles.header}>
          <h5>Réservation d&apos;un créneau</h5>
          <a href="#" onClick={handleCloseClick}>
            x
          </a>
        </div>
        <form
          onSubmit={handleCreateBooking}
          method="post"
          className="mt-1 ml-3"
        >
          <div className="mt-2">
            <label>Période</label>
            <DateRange
              ranges={[selectionRange]}
              onChange={handleSelectDate}
              minDate={new Date()}
              disabledDates={disabledDates}
            />
          </div>
          <div className="mt-2">
            <label>Prix {price}€</label>
          </div>
          <div className="mt-5">
            <button className="mr-5 btn-secondary" onClick={handleCloseClick}>
              Annuler
            </button>
            <button className="btn btn btn-primary" type="submit">
              Réserver ce créneaux
            </button>
          </div>
        </form>
      </div>
    </div>
  ) : null;

  if (isBrowser) {
    return modalContent;
  } else {
    return null;
  }
};

export default BookingModal;
