import Link from "next/link";

export default function ProfileNav() {
  return (
    <div className="d-flex flex-column ml-5 mr-5">
      <Link href={"/profile/informations"}>
        <a className="m-2">Mes informations</a>
      </Link>
      <Link href={"/profile/annonces"}>
        <a className="m-2">Mes annonces</a>
      </Link>
      <Link href={"/profile/documents"}>
        <a className="m-2">Mes documents</a>
      </Link>
      <Link href={"/profile/reservations"}>
        <a className="m-2">Mes réservations</a>
      </Link>
    </div>
  );
}
