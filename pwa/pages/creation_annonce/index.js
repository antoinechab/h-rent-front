import { useState } from "react";
import styles from "./CreationAnnonce.module.css";
import { useSession, signIn } from "next-auth/react";
import axios from "axios";
import { useRouter } from "next/router";

export default function AnnonceCreation() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [capacity, setCapacity] = useState(1);

  const [address, setAddress] = useState("");
  const [adAddress, setAdAddress] = useState("");
  const [city, setCity] = useState("");
  const [zipCode, setZipCode] = useState("");
  const [country, setCountry] = useState("");

  const { data, status } = useSession();
  const [images, setImages] = useState([]);

  const router = useRouter();

  const submitForm = async (event) => {
    event.preventDefault();

    let response = await fetch("https://hesias-rent.herokuapp.com/addresses", {
      method: "POST",
      body: JSON.stringify({
        address,
        additionalAddress: adAddress,
        city,
        zipCode,
        country,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${data.accessToken}`,
      },
    });

    const addressData = await response.json();

    const iriRenter = `/users/${data.user.id}`;
    const iriAddress = `/addresses/${addressData.id}`;

    const body = JSON.stringify({
      title: title,
      description: description,
      capacity: Number(capacity),
      address: iriAddress,
      renter: iriRenter,
    });
    console.log("BODY", body);

    response = await fetch("https://hesias-rent.herokuapp.com/annonces", {
      method: "POST",
      body: body,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${data.accessToken}`,
      },
    });

    const annonceData = await response.json();
    console.log(annonceData);

    const principal = true;
    for (let image of images) {
      const formData = new FormData();

      formData.append("annonce", annonceData.id);
      formData.append("name", image.name);
      formData.append("file", image);
      formData.append("principal", principal);

      axios
        .post("https://hesias-rent.herokuapp.com/files", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${data.accessToken}`,
          },
        })
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
      principal = false;
    }

    router.push("/annonces?page=1");
  };

  const uploadToClient = (event) => {
    if (event.target.files) {
      setImages(event.target.files);
    }
  };

  if (status != "authenticated") {
    return <p>Vous devez être connecté</p>;
  } else {
    return (
      <div className={styles.container}>
        <form className="column col-8" onSubmit={submitForm}>
          <h3>Création d&apos;une annonce</h3>
          <div className="col-12">
            <label className="form-label">Titre</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
          <div className="col-12">
            <label className="form-label">Description</label>
            <textarea
              className="form-control"
              id="description"
              rows="3"
              required
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </div>
          <div className="col-md-3">
            <label className="form-label">Capacité</label>
            <input
              type="number"
              className="form-control"
              id="capacity"
              required
              min={1}
              value={capacity}
              onChange={(e) => setCapacity(e.target.value)}
            />
          </div>
          <hr></hr>
          <div className="col-12">
            <label className="form-label">Adresse</label>
            <input
              type="text"
              className="form-control"
              id="address"
              placeholder="1234 Main St"
              required
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <div className="col-12">
            <label className="form-label">Complement</label>
            <input
              type="text"
              className="form-control"
              id="adAddress"
              value={adAddress}
              onChange={(e) => setAdAddress(e.target.value)}
            />
          </div>
          <div className={styles.address}>
            <div className="col-md-6">
              <label className="form-label">Ville</label>
              <input
                type="text"
                className="form-control"
                id="city"
                required
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
            </div>
            <div className="col-md-2">
              <label className="form-label">Code postal</label>
              <input
                type="text"
                className="form-control"
                id="zipCode"
                required
                value={zipCode}
                onChange={(e) => setZipCode(e.target.value)}
              />
            </div>
          </div>
          <div className="col-md-6">
            <label className="form-label">Pays</label>
            <input
              type="text"
              className="form-control"
              id="country"
              required
              value={country}
              onChange={(e) => setCountry(e.target.value)}
            />
          </div>
          <div className="col-md-6">
            <label>Sélectionner des images</label>
            <input
              required
              className="form-control"
              multiple
              accept="image/*"
              type="file"
              name="file"
              id="file"
              onChange={(e) =>
                e.target.files.length <= 5
                  ? uploadToClient(e)
                  : alert("5 images maximum")
              }
            />
          </div>

          <div className="col-12 mt-3">
            <button type="submit" className="btn btn-primary">
              Créer l&apos;annonce
            </button>
          </div>
        </form>
      </div>
    );
  }
}
