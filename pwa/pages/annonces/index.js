import AnnonceComponent from "../../components/AnnonceComponent";
import SearchBarComponent from "../../components/SearchBarComponent";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function Annonces({ annonces }) {
  const router = useRouter();

  useEffect(() => {
    if (router.asPath == "/annonces") {
      router.push("/annonces?page=1");
    }
  });

  const selectOrder = (value) => {
    let newPath = router.asPath.replace("&order[capacity]=asc", "");
    newPath = newPath.replace("&order[capacity]=desc", "");
    router.push(`${newPath}${value}`);
  };

  return (
    <div className="container-xxl ml-5">
      <h3>Liste des annonces</h3>
      <SearchBarComponent></SearchBarComponent>
      <div>
        <div className="container">
          <select className="form-select mb-5" id="distance">
            <option
              value=""
              onClick={(e) => selectOrder(e.target.value)}
              defaultValue
            >
              Aucun tri
            </option>
            <option
              value="&order[capacity]=asc"
              onClick={(e) => selectOrder(e.target.value)}
            >
              Tri: Capacité croissante
            </option>
            <option
              value="&order[capacity]=desc"
              onClick={(e) => selectOrder(e.target.value)}
            >
              Tri: Capacité decroissantes
            </option>
          </select>

          {annonces.length > 0 ? (
            annonces.map((annonce, index) => (
              <AnnonceComponent
                key={`${index}-${annonce.id}`}
                annonce={annonce}
              />
            ))
          ) : (
            <div>Aucun résultat</div>
          )}
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(query) {
  if (query.query.city) {
    // TODO Appeler geoloc sinon appeler route normal
    var geolocId = await fetch(
      `https://hesias-rent.herokuapp.com/annonces/geolocation?page=1&geoloc_radius=${query.query.city}%2B${query.query.distance}`
    ).then((r) => r.json());
  }
  let annonces = await fetch(
    `https://hesias-rent.herokuapp.com${query.resolvedUrl}`
  ).then((r) => r.json());

  if (geolocId) {
    const newAnnonces = [];
    annonces.map((annonce) => {
      geolocId.map((id) => {
        if (annonce.address.id == id) {
          newAnnonces.push(annonce);
        }
      });
    });
    annonces = newAnnonces;
    return {
      props: {
        annonces,
      },
    };
  } else {
    return {
      props: {
        annonces,
      },
    };
  }
}
