import styles from "./Annonce.module.css";
import DefaultPicture from "../../assets/accueilbis.jpg";
import Image from "next/image";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState } from "react";
import dynamic from "next/dynamic";
import SlotModal from "../../components/SlotModal";
import { format } from "date-fns";
import { useEffect } from "react";
import BookingModal from "../../components/BookingModal";
import Paypal from "../../components/PayPal/Paypal";

export default function Annonce({ annonce }) {
  const MapWithNoSSR = dynamic(() => import("../../components/Map"), {
    ssr: false,
  });

  const { data, status } = useSession();

  const router = useRouter();
  const [images, setImages] = useState();
  const [showModal, setShowModal] = useState(false);
  const [showBookingModal, setShowBookingModal] = useState(false);

  const back = () => {
    router.back();
  };

  //recuperation des images sur le serveur

  // useEffect(() => {
  //   async function fetchData() {
  //     if (status == "authenticated") {
  //       const image = await fetch(
  //         `https://hesias-rent.herokuapp.com/uploads/files/628d34d89bf46_3.jpeg`,
  //         {
  //           headers: {
  //             Authorization: `Bearer ${data.accessToken}`,
  //           },
  //         }
  //       );
  //     }
  //   }
  //   fetchData();
  // }, [annonce.pictures, data, status]);

  // const myLoader = ({ src, width, quality }) => {
  //   return `https://hesias-rent.herokuapp.com/uploads/files/${src}?w=${width}&q=${
  //     quality || 75
  //   }`;
  // };

  const validateBooking = async () => {
    const newBooking = await fetch(
      `https://hesias-rent.herokuapp.com/uploads/files/628d34d89bf46_3.jpeg`,
      {
        headers: {
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    );
  };

  if (annonce.detail == "Not Found") {
    return <>Annonce inéxistante</>;
  } else {
    let map;
    if (annonce.address.longitude && annonce.address.latitude) {
      map = (
        <MapWithNoSSR
          latitude={annonce.address.latitude}
          longitude={annonce.address.longitude}
        />
      );
    } else {
      map = <p>Map indisponible pour cette annonce.</p>;
    }
    return (
      <div className={styles.container}>
        <h2>Detail de l&apos;annonce</h2>
        <a href="#" onClick={back}>
          Revenir aux annonces
        </a>
        <div className={styles.top}>
          <div className={styles.img}>
            {/* <Image
            loader={myLoader}
            src={annonce.pictures[1].path}
            alt=""
            width="200"
            height="200"
          /> */}
            <Image src={DefaultPicture} alt="" width="400" height="300" />
          </div>
          <div className={styles.information}>
            <div>
              <h3>{annonce.title}</h3>
              <p>{annonce.description}</p>
            </div>
            <p>Capacité: {annonce.capacity}</p>
          </div>
        </div>
        <hr></hr>
        <div className={styles.address}>
          <div className={styles.address_info}>
            <h3>Localisation</h3>
            <p> {annonce.address.address} </p>
            <p>
              {annonce.address.zipCode}, {annonce.address.city}
            </p>
            <p> {annonce.address.country} </p>
          </div>
          <div id="map" className={styles.address_map}>
            {map}
          </div>
        </div>

        {status == "authenticated" ? (
          <>
            {data.user.id != annonce.renter.id ? (
              <div className="d-flex flex-column justify-content-center mt-5">
                <button
                  className="btn btn-primary"
                  onClick={() => setShowBookingModal(true)}
                >
                  Reserver
                </button>
                <BookingModal
                  show={showBookingModal}
                  annonce={annonce}
                  myId={data.user.id}
                  accessToken={data.accessToken}
                  onClose={() => setShowBookingModal(false)}
                  slots={annonce.slots}
                  onValidate={() => {
                    setShowBookingModal(false);
                  }}
                />
              </div>
            ) : (
              <div className="mt-5">
                <h3>Liste des créneaux</h3>
                <button
                  className="btn btn-primary mb-4"
                  onClick={() => setShowModal(true)}
                >
                  Ajouter un créneau
                </button>
                <SlotModal
                  show={showModal}
                  annonceId={annonce.id}
                  accessToken={data.accessToken}
                  onClose={() => setShowModal(false)}
                  onCreate={() => router.reload()}
                  slots={annonce.slots}
                />
                <div>
                  {annonce.slots.length > 0 ? (
                    annonce.slots.map((slot, index) => (
                      <div
                        key={`slot${index}`}
                        className="d-flex flex-column border mt-2 "
                      >
                        <div>
                          <label>Date de début: </label>
                          <label>
                            {format(new Date(slot.startDate), "dd/MM/yyyy")}
                          </label>
                        </div>
                        <div>
                          <label>Date de fin: </label>
                          <label>
                            {format(new Date(slot.endDate), "dd/MM/yyyy")}
                          </label>
                        </div>
                        <div>
                          <label>Prix: </label>
                          <label>{slot.price} €/nuit</label>
                        </div>
                      </div>
                    ))
                  ) : (
                    <div>Aucun slot encore créé.</div>
                  )}
                </div>
              </div>
            )}
          </>
        ) : null}
      </div>
    );
  }
}

export async function getServerSideProps({ params }) {
  const annonce = await fetch(
    `https://hesias-rent.herokuapp.com/annonces/${params.id}`
  ).then((r) => r.json());

  return {
    props: {
      annonce,
    },
  };
}
