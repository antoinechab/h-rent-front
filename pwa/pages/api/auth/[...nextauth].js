import nextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import jwt_decode from "jwt-decode";

export default nextAuth({
  providers: [
    CredentialsProvider({
      credentials: {
        email: {
          label: "Email",
          type: "text ",
          placeholder: "jsmith@example.com",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const res = await fetch("https://hesias-rent.herokuapp.com/login", {
          method: "POST",
          body: JSON.stringify(credentials),
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        });
        const user = await res.json();

        if (res.ok && user) {
          return user;
        }
        return null;
      },
    }),
  ],
  secret: "secret",
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        console.log("");
        const decoded = jwt_decode(user.token);
        token.accessToken = user.token;
      }
      return token;
    },
    async session({ session, token }) {
      session.accessToken = token.accessToken;
      if (session?.user) {
        const decoded = jwt_decode(token.accessToken);
        session.user.id = decoded.id;
      }
      return session;
    },
  },
  theme: { colorScheme: "light" },
});
