import { signIn, useSession } from 'next-auth/react';
import { useState } from 'react';
import styles from './SignUp.module.css'
import { useRouter } from 'next/router';

export default function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [pseudo, setPseudo] = useState("");

    const { data, status } = useSession();
    const router = useRouter();


    const handleSubmit = async (event) => {
        event.preventDefault();
        if (password != confirmPassword) {
            alert("Les mots de passes ne correspondent pas")
            return;
        }

        const data = JSON.stringify({ email, password, firstName, lastName, pseudo });

        const res = await fetch('https://hesias-rent.herokuapp.com/users', {
            method: 'POST',
            body: data,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        })

        const user = await res.json();

        signIn("credentials", { email, password, callbackUrl: '/' })
    }

    if (status == "authenticated") {
        router.push('/');
    } else {


        return (

            <div className={styles.container}>
                <form onSubmit={handleSubmit} className={styles.form} method="post">
                    <h3 className={styles.page_title}>Création du compte</h3>
                    <div className={styles.formContent}>
                        <div className={styles.annonce}>

                            <label className={styles.label}>
                                Nom
                                <input
                                    name="lastName"
                                    type="text"
                                    className="form-control"
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}
                                    required />
                            </label>

                            <label className={styles.label}>
                                Prénom
                                <input
                                    name="firstName"
                                    type="text"
                                    className="form-control"
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}
                                    required />
                            </label>

                            <label className={styles.label}>
                                Pseudo
                                <input
                                    name="pseudo"
                                    type="text"
                                    value={pseudo}
                                    className="form-control"
                                    onChange={e => setPseudo(e.target.value)}
                                    required />
                            </label>

                            <label className={styles.label}>
                                Email
                                <input
                                    name="email"
                                    type="email"
                                    className="form-control"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required />
                            </label>

                            <label className={styles.label}>
                                Mot de passe
                                <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    required />
                            </label>

                            <label className={styles.label}>
                                Validation mot de passe
                                <input
                                    name="confirmPassword"
                                    type="password"
                                    className="form-control"
                                    value={confirmPassword}
                                    onChange={e => setConfirmPassword(e.target.value)}
                                    required />
                            </label>
                        </div>
                    </div>
                    <button className="btn btn-primary" type='submit'>S&apos;inscrire</button>
                </form>

            </div>
        )
    }
}