import Head from "next/head";
import SearchBarComponent from "../components/SearchBarComponent";
import villa from "../assets/villa.jpg";
import accueiljpg from "../assets/accueil.jpg";
import Image from "next/image";
import AnnonceComponent from "../components/AnnonceComponent";

export default function Home({ annonceCouple, annonceAmis }) {
  return (
    <div className="overflow-hidden">
      <Head>
        <title>H-Rent</title>
      </Head>
      <div>
        <div className="d-flex overflow-hidden">
          <Image src={accueiljpg} alt="" width={960} height={400} />
          <Image src={villa} alt="" width={960} height={400} />
        </div>
        <div className="container-xxl">
          <SearchBarComponent />
        </div>

        <div className="d-flex flex-row justify-content-around">
          <div style={{width:'800px'}}>
            <h4>En couple ?</h4>
            <AnnonceComponent annonce={annonceCouple} />
          </div>
          <div style={{width:'800px'}}>
            <h4>Entre amis ?</h4>
            <AnnonceComponent annonce={annonceAmis} />
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps() {
  const annonceCouple = await fetch(
    "https://hesias-rent.herokuapp.com/annonces?page=1&capacity=2"
  ).then((r) => r.json());

  const annonceAmis = await fetch(
    "https://hesias-rent.herokuapp.com/annonces?page=1&order[capacity]=desc"
  ).then((r) => r.json());

  return {
    props: {
      annonceCouple: annonceCouple[0],
      annonceAmis: annonceAmis[0],
    },
  };
}
