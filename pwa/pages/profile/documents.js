import { useSession } from "next-auth/react";
import ProfileNav from "../../components/ProfileNav";

export default function MesDocuments() {
  const { data, status } = useSession();

  if (status == "authenticated") {
    return (
      <div className="d-flex mt-5">
        <ProfileNav></ProfileNav>
        <div className="container">
          <h3 className="mb-3">Mes documents</h3>
        </div>
      </div>
    );
  } else {
    return <p>Vous devez être connecté</p>;
  }
}
