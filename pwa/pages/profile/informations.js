import { useSession, signIn } from "next-auth/react";
import { useState, useEffect } from "react";
import ProfileNav from "../../components/ProfileNav";
import styles from "./Profile.module.css";

export default function Profile() {
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [pseudo, setPseudo] = useState("");
  const [paypal, setPaypal] = useState("");

  const { data, status } = useSession();

  useEffect(() => {
    async function fetchData() {
      if (status == "authenticated") {
        const user = await fetch(
          `https://hesias-rent.herokuapp.com/users/${data.user.id}`,
          {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          }
        ).then((r) => r.json());
        setEmail(user.email);
        setFirstName(user.firstName);
        setLastName(user.lastName);
        setPseudo(user.pseudo);

        const paypalInfo = await fetch(
          `https://hesias-rent.herokuapp.com/paypal_informations?page=1&owner.id=${data.user.id}`,
          {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          }
        ).then((r) => r.json());
        if (paypalInfo[0]) {
          setPaypal(paypalInfo[0].email);
        }
      }
    }
    fetchData();
  }, [data, status]);

  const updatePaypal = async (event) => {
    event.preventDefault();
    console.log(paypal);
    const paypalInfo = await fetch(
      `https://hesias-rent.herokuapp.com/paypal_informations`,
      {
        method: "POST",
        body: JSON.stringify({
          owner: `/users/${data.user.id}`,
          email: paypal,
          principal: true,
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    ).then((r) => r.json());
  };

  if (status == "authenticated") {
    return (
      <div className="d-flex mt-5">
        <ProfileNav></ProfileNav>
        <div className={styles.container}>
          <form className="column col-12" method="post">
            <h3>Mes informations</h3>
            <div className="col-12">
              <label className="form-label">
                Nom
                <input
                  name="lastName"
                  type="text"
                  disabled
                  className="form-control"
                  value={lastName}
                  required
                />
              </label>
            </div>
            <div className="col-12">
              <label className="form-label">
                Prénom
                <input
                  name="firstName"
                  disabled
                  type="text"
                  className="form-control"
                  value={firstName}
                  required
                />
              </label>
            </div>

            <div className="col-12">
              <label className="form-label">
                Pseudo
                <input
                  name="pseudo"
                  disabled
                  type="text"
                  value={pseudo}
                  className="form-control"
                  required
                />
              </label>
            </div>

            <div className="col-12">
              <label className="form-label">
                Email
                <input
                  name="email"
                  disabled
                  type="email"
                  className="form-control"
                  value={email}
                  required
                />
              </label>
            </div>

            <div className="col-12">
              <label className="form-label">
                Email
                <input
                  name="paypal"
                  type="email"
                  className="form-control"
                  value={paypal}
                  onChange={(e) => setPaypal(e.target.value)}
                  required
                />
                <button onClick={(e) => updatePaypal(e)}>
                  Modifier email paypal
                </button>
              </label>
            </div>
          </form>
        </div>
      </div>
    );
  } else {
    return <p>Vous devez être connecté</p>;
  }
}

// export async function getServerSideProps() {

//     const user = await fetch(
//         `https://hesias-rent.herokuapp.com/users/`
//     ).then((r) => r.json());

//     return {
//         props: {
//             user,
//         },
//     };
// }
