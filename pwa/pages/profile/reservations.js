import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import Paypal from "../../components/PayPal/Paypal";
import ProfileNav from "../../components/ProfileNav";

export default function Reservations() {
  const { data, status } = useSession();
  const [bookings, setBookings] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchData() {
      if (status == "authenticated") {
        const bookingsData = await fetch(
          `https://hesias-rent.herokuapp.com/bookings?page=1&tenant.id=${data.user.id}`,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${data.accessToken}`,
            },
          }
        ).then((r) => r.json());
        setBookings(bookingsData);

        let bookingWithPrice = [];
        for (let booking of bookingsData) {
          let price = 0;

          for (let slot of booking.slots) {
            let slotData = await fetch(
              `https://hesias-rent.herokuapp.com${slot}`,
              {
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${data.accessToken}`,
                },
              }
            ).then((r) => r.json());
            price = price + slotData.price;
          }
          booking.price = price;
          bookingWithPrice.push(booking);
        }
        setBookings(bookingWithPrice);
      }
    }
    fetchData();
  }, [data, status]);

  const paiementValidate = async (bookingId) => {
    let response = await fetch(
      `https://hesias-rent.herokuapp.com/bookings/${bookingId}`,
      {
        method: "PATCH",
        body: JSON.stringify({
          statut: 1,
        }),
        headers: {
          "Content-Type": "application/merge-patch+json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    );

    const bookingData = await response.json();
    console.log(bookingData);

    router.reload();
  };

  const cancelBooking = async (event, booking) => {
    event.preventDefault();
    let response = await fetch(
      `https://hesias-rent.herokuapp.com/bookings/${booking.id}`,
      {
        method: "PATCH",
        body: JSON.stringify({
          statut: 3,
        }),
        headers: {
          "Content-Type": "application/merge-patch+json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    );

    const bookingData = await response.json();
    console.log(bookingData);

    router.reload();
  };

  if (status == "authenticated") {
    return (
      <div className="d-flex mt-5">
        <ProfileNav></ProfileNav>
        <div className="container">
          <h3 className="mb-3">Mes réservations</h3>
          {bookings.length > 0 ? (
            bookings.map((booking, index) => (
              <div
                key={`booking${index}`}
                className="border d-flex justify-content-between"
              >
                <label>Reservation pour {booking.slots.length} jours</label>
                {booking.statut == -1 && (
                  <label>Statut: Attente validation propriétaire</label>
                )}
                {booking.statut == 0 && (
                  <label>Statut: Attente de paiement locataire</label>
                )}
                {booking.statut == 1 && (
                  <label>
                    Statut: Payé, en attente validation propriétaire
                  </label>
                )}
                {booking.statut == 3 && <label>Statut: Annulé</label>}
                {booking.statut == 4 && <label>Statut: Remboursé</label>}
                {booking.statut == 5 && <label>Statut: Validé</label>}
                <div>
                  <button
                    onClick={(e) => cancelBooking(e, booking)}
                    className="btn-secondary"
                  >
                    Annuler
                  </button>
                  {booking.statut == 0 && booking.price != undefined && (
                    <Paypal
                      booking={booking}
                      accessToken={data.accessToken}
                      price={Number(booking.price)}
                      onPaymentValidate={() => paiementValidate(booking.id)}
                    />
                  )}
                </div>
              </div>
            ))
          ) : (
            <label>Pas de réservation pour cette annonce</label>
          )}
        </div>
      </div>
    );
  } else {
    return <p>Vous devez être connecté</p>;
  }
}
