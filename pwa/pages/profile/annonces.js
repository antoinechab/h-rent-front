import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import AnnonceComponent from "../../components/AnnonceComponent";
import ProfileNav from "../../components/ProfileNav";

export default function MesAnnonces() {
  const { data, status } = useSession();
  const [annonces, setAnnonces] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchData() {
      if (status == "authenticated") {
        const annoncesData = await fetch(
          `https://hesias-rent.herokuapp.com/annonces?page=1&renter.id=${data.user.id}`
        ).then((r) => r.json());
        setAnnonces(annoncesData);
      }
    }
    fetchData();
  }, [data, status]);

  const validationBeforePaiement = async (event, booking) => {
    event.preventDefault();
    console.log(booking);

    let response = await fetch(
      `https://hesias-rent.herokuapp.com/bookings/${booking.id}`,
      {
        method: "PATCH",
        body: JSON.stringify({
          statut: 0,
        }),
        headers: {
          "Content-Type": "application/merge-patch+json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    );

    const bookingData = await response.json();
    console.log(bookingData);

    router.reload();
  };

  const validationAfterPaiement = async (event, booking) => {
    event.preventDefault();
    console.log("validate");
    const bookingsData = await fetch(
      `https://hesias-rent.herokuapp.com/bookings/${booking.id}/validate`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    ).then((r) => r.json());

    console.log(bookingsData);
  };

  const cancelBooking = async (event, booking) => {
    event.preventDefault();
    let response = await fetch(
      `https://hesias-rent.herokuapp.com/bookings/${booking.id}`,
      {
        method: "PATCH",
        body: JSON.stringify({
          statut: 3,
        }),
        headers: {
          "Content-Type": "application/merge-patch+json",
          Authorization: `Bearer ${data.accessToken}`,
        },
      }
    );

    const bookingData = await response.json();
    console.log(bookingData);

    router.reload();
  };

  if (status == "authenticated") {
    return (
      <div className="d-flex mt-5">
        <ProfileNav></ProfileNav>
        <div className="container">
          <h3 className="mb-3">Mes annonces</h3>
          {annonces.length > 0 ? (
            annonces.map((annonce, index) => (
              <div key={`${index}-${annonce.id}`} className="mt-5">
                <AnnonceComponent annonce={annonce} />
                {annonce.bookings.length > 0 ? (
                  annonce.bookings.map((booking, index) => (
                    <div
                      key={`booking${index}`}
                      className="border d-flex justify-content-between"
                    >
                      <label>
                        Reservation pour {booking.slots.length} jours
                      </label>
                      {booking.statut == -1 && (
                        <label>Statut: Attente validation propriétaire</label>
                      )}
                      {booking.statut == 0 && (
                        <label>Statut: Attente de paiemenet locataire</label>
                      )}
                      {booking.statut == 1 && (
                        <label>
                          Statut: Payé, en attente validation propriétaire
                        </label>
                      )}
                      {booking.statut == 3 && <label>Statut: Annulé</label>}
                      {booking.statut == 4 && <label>Statut: Remboursé</label>}
                      {booking.statut == 5 && <label>Statut: Validé</label>}
                      <div>
                        <button
                          onClick={(e) => cancelBooking(e, booking)}
                          className="btn-secondary"
                        >
                          Annuler
                        </button>
                        {booking.statut == -1 && (
                          <button
                            onClick={(e) =>
                              validationBeforePaiement(e, booking)
                            }
                            className="btn btn-primary"
                          >
                            Valider
                          </button>
                        )}
                        {booking.statut == 1 && (
                          <button
                            onClick={(e) => validationAfterPaiement(e, booking)}
                            className="btn btn-primary"
                          >
                            Valider
                          </button>
                        )}
                      </div>
                    </div>
                  ))
                ) : (
                  <label>Pas de réservation pour cette annonce</label>
                )}
              </div>
            ))
          ) : (
            <div>Aucun résultat</div>
          )}
        </div>
      </div>
    );
  } else {
    return <p>Vous devez être connecté</p>;
  }
}
