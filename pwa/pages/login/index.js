import Link from "next/link";
import { useState } from "react";
import { useRouter } from "next/router";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const router = useRouter();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch('https://hesias-rent.herokuapp.com/login', {
      method: 'POST',
      body: JSON.stringify({ email, password }),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((r) => r.json());
    const data = await response;
    if (data.token) {
      await router.push("/");

      if (data.message) {
        alert(data.message);
      }
    };
  }

    return (
      <form className="container" onSubmit={handleSubmit} method='post'>
        <h3>Connexion</h3>
        <div className="mb-3 row">
          <label className="col-sm-2 col-form-label">
            Email
          </label>
          <div className="col-sm-6">
            <input
              className="form-control"
              id="email"
              name="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
        </div>
        <div className="mb-3 row">
          <label className="col-sm-2 col-form-label">
            Password
          </label>
          <div className="col-sm-6">
            <input
              className="form-control"
              name="password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
        </div>
        <div className="mb-3 row">
          <button type="submit" className="btn btn-primary mb-3">
            Connexion
          </button>
        </div>
        <Link href="/sign_in">
          <a>Créer un compte</a>
        </Link>
      </form>
    );
  }
